import React from 'react';
import CustomButton from '../Button/Button';
import { Modal } from 'react-bootstrap';

const CustomModal = (props) => {
    return (
        <React.Fragment>
            <Modal
                {...props}
                aria-labelledby="contained-modal-title-vcenter"
                centered
            >
                <Modal.Header closeButton>
                    <Modal.Title id="contained-modal-title-vcenter">
                        {props.heading}
                    </Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <p>{props.content}</p>
                    <div>{props.children}</div>
                </Modal.Body>
                <Modal.Footer>
                    {props.buttons && props.buttons.length ?
                        props.buttons.map((button, index) => (
                            <CustomButton btnType={button.btnType} key={index.toString()} clicked={() => button.handler(props.data)}>{button.label}</CustomButton>
                        ))
                        : <CustomButton clicked={props.onHide}>Close</CustomButton>}

                </Modal.Footer>
            </Modal>
        </React.Fragment>
    )
}

export default React.memo(CustomModal);