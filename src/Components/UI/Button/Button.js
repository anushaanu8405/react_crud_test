import React from 'react';
import { Button } from 'react-bootstrap';

const CustomButton = (props) => (
    <Button
        className={props.className}
        variant={props.btnType ? props.btnType : 'primary'}
        onClick={props.clicked}
        disabled={props.disabled}
        size={props.btnSize}>{props.children}</Button>
);

export default React.memo(CustomButton); // React.memo - To render DOM Only to props change