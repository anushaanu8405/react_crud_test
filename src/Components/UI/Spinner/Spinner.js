import React from 'react';
import { Spinner, Button } from 'react-bootstrap';

import './Spinner.css';

const CustomSpinner = () => (
    <div className="Overlay">
        <div className='Loader'>
            <Button variant="primary" disabled>
                <Spinner
                    as="span"
                    animation="border"
                    size="lg"
                    role="status"
                    aria-hidden="true"
                />
            </Button>
        </div>
    </div>
);

export default React.memo(CustomSpinner);