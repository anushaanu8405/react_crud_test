import React from 'react';

import './Input.css';

const CustomInput = (props) => {
    const inputClasses = ['form-control'];
    if (props.invalid && props.shouldValidate && props.touched) {
        inputClasses.push('Invalid');
    }

    return (
        <div className="col-sm-12 mb-2" >
            <label htmlFor={props.label} className="mb-0">{props.label}</label>
            <input id={props.label}
                className={inputClasses.join(' ')}
                {...props.elementConfig}
                value={props.value}
                onChange={props.changed} />

            {props.invalid && props.touched ?
                <div className="InvalidText" >
                    {!props.value ? 'This field is required' : props.shouldValidate.errorMessage}
                </div>
                : null}

        </div>
    );
};

export default React.memo(CustomInput);