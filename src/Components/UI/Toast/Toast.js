

import React from 'react';
import { Toast } from 'react-bootstrap';
import './Toast.css';

const CustomToast = (props) => {

    return (
        <Toast className='ToastView' delay={2000} autohide show={props.show} onClose={props.hideToast}>
            <Toast.Header>
                <strong className="mr-auto">{props.toastHeading}</strong>
            </Toast.Header>
            <Toast.Body>{props.toastContent}</Toast.Body>
        </Toast>
    )
}

export default React.memo(CustomToast);