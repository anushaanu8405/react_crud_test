import React from 'react';
import logo from './logo.svg';
import './App.css';
import ErrorBoundary from './Containers/ErrorBoundary';
import Applications from './Containers/Applications/Applications';

function App() {
  return (
    <div className="App">

      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
      </header>

      <div className="container">
        <ErrorBoundary><Applications /></ErrorBoundary>
      </div>

    </div>
  );
}

export default App;
