export const messages = {
    serverErrorMsg: 'Oops... Something went wrong!',
    errorHeadingMsg: 'Error',
    successHeadingMsg: 'Success',
    applicationDeleteFailedMsg: 'Unable to delete application. Try again later',
    applicationDeleteSuccessMsg: 'Application deleted successfully',
    applicationCreateFailedMsg: 'Unable to create application. Try again later',
    applicationCreateSuccessMsg: 'Application created successfully',
}