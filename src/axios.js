import axios from 'axios';

const instance = axios.create({
    baseURL: 'https://5ef6eb3f2c0f2c0016949dbc.mockapi.io/'
});

export default instance;