import React, { PureComponent } from 'react';
import { Table } from 'react-bootstrap';
import Moment from 'moment';


import './Applications.css';
import axios from '../../axios';
import CustomButton from '../../Components/UI/Button/Button';
import CustomModal from '../../Components/UI/Modal/Modal';
import CustomSpinner from '../../Components/UI/Spinner/Spinner';
import CustomToast from '../../Components/UI/Toast/Toast';
import CustomInput from '../../Components/UI/Input/Input';
import { messages } from '../../Config/Messages';
import { APINames } from '../../Config/APINames';

const initialFormObj = {
    name: {
        label: 'Full Name',
        elementConfig: {
            type: 'text',
            placeholder: 'Your name'
        },
        value: '',
        validation: {
            required: true,
            minLength: 3,
            maxLength: 30,
            errorMessage: ''
        },
        valid: false,
        touched: false
    },
    email: {
        label: 'Email',
        elementConfig: {
            type: 'email',
            placeholder: 'Your email'
        },
        value: '',
        validation: {
            required: true,
            isEmail: true,
            errorMessage: ''
        },
        valid: false,
        touched: false
    },
    resumeUrl: {
        label: 'Resume URL',
        elementConfig: {
            type: 'url',
            placeholder: 'Your resume url'
        },
        value: '',
        validation: {
            required: true,
            isUrl: true,
            errorMessage: ''
        },
        valid: false,
        touched: false
    },
};
class Applications extends PureComponent {
    state = {
        applications: [],
        loading: true,
        isOpenModal: false,
        modalHeading: '',
        modalData: '',
        modalContent: '',
        modalButtons: [],
        toastHeading: '',
        toastContent: '',
        isDisplayToast: false,
        applicationColumns: [
            { label: 'S.No', value: 'serialNo' },
            { label: 'Name', value: 'name' },
            { label: 'Email', value: 'email' },
            { label: 'Resume URL', value: 'resumeUrl' },
            { label: 'Created At', value: 'createdAt' },
            { label: '', value: 'action' }
        ],
        applicationForm: { ...initialFormObj },
        formIsValid: false,
    }

    componentDidMount() {
        this.getApplicationsList();
    }

    // get applications list
    getApplicationsList = () => {
        this.setState({ loading: true }, () => {
            axios.get(APINames.applicationAPI)
                .then(res => {
                    this.setState({
                        loading: false,
                        applications: res.data && res.data.length ? res.data : []
                    });
                })
                .catch(() => {
                    this.setState({ loading: false });
                });
        })
    }

    // display delete confirmation popup 
    askDeleteConfirmation = (application) => {
        this.setState({
            isOpenModal: true,
            modalHeading: 'Delete Confirmation',
            modalContent: `Are you sure you want to delete ${application.name}?`,
            modalData: { id: application.id },
            modalButtons: [
                {
                    label: 'Yes',
                    handler: (modalData) => {
                        this.deleteApplicationHandler(modalData.id);
                    }
                },
                {
                    label: 'No',
                    btnType: 'secondary',
                    handler: () => {
                        this.hideModal();
                    }
                },
            ],
        });
    }

    hideModal = () => {
        this.setState({ isOpenModal: false, loading: false });
    }

    // application delete handler
    deleteApplicationHandler = (id) => {
        this.setState({ loading: true });
        axios.delete(`${APINames.applicationAPI}/${id}`)
            .then(res => {
                if (res && res.status && res.status === 200) {
                    this.hideModal();
                    this.getApplicationsList();
                    this.dismissLoaderAndDisplayMsg(messages.successHeadingMsg, messages.applicationDeleteSuccessMsg);
                } else {
                    this.dismissLoaderAndDisplayMsg(messages.errorHeadingMsg, messages.applicationDeleteFailedMsg);
                }
            })
            .catch(() => {
                this.dismissLoaderAndDisplayMsg(messages.errorHeadingMsg, messages.serverErrorMsg);
            });
    }

    // dismiss load and display message
    dismissLoaderAndDisplayMsg(heading, errorMessage) {
        this.setState({
            loading: false,
            isDisplayToast: true,
            toastHeading: heading,
            toastContent: errorMessage
        });
    }

    // Open new application popup
    openNewApplicationPopup = () => {
        this.setState({
            applicationForm: JSON.parse(JSON.stringify(initialFormObj)), // Deep cloning 
            formIsValid: false,
            loading: false,
            isOpenModal: true,
            modalHeading: 'Create Application',
            modalContent: '',
            modalData: {},
            modalButtons: [
                {
                    label: 'Create',
                    handler: () => {
                        this.addNewApplication();
                    }
                },
                {
                    label: 'Cancel',
                    btnType: 'secondary',
                    handler: () => {
                        this.hideModal();
                    }
                },
            ],
        });
    }

    // Check every input field validity with proper validations
    checkValidity(value, rules) {
        rules.errorMessage = '';
        let isValid = true;
        if (!rules) {
            return true;
        }

        if (rules.required) {
            isValid = value.trim() !== '' && isValid;
        }

        if (rules.minLength) {
            isValid = value.length >= rules.minLength && isValid;
            if (!isValid) {
                rules.errorMessage = `This field should contain atleast ${rules.minLength} characters`;
            }
        }

        if (rules.maxLength) {
            isValid = value.length <= rules.maxLength && isValid;
            if (!isValid && !rules.errorMessage) {
                rules.errorMessage = `Maximum of ${rules.maxLength} characters are allowed.`;
            }
        }

        if (rules.isEmail) {
            const pattern = /[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?/;
            isValid = pattern.test(value) && isValid
            rules.errorMessage = 'Please enter valid email address';
        }

        if (rules.isUrl) {
            const pattern = /^((https?|ftp|smtp):\/\/)?(www.)?[a-z0-9]+(\.[a-z]{2,}){1,3}(#?\/?[a-zA-Z0-9#]+)*\/?(\?[a-zA-Z0-9-_]+=[a-zA-Z0-9-%]+&?)?$/;
            isValid = pattern.test(value) && isValid
            rules.errorMessage = 'Please enter valid url';
        }

        return isValid;
    }

    // Handle every input change
    inputChangedHandler = (event, inputIdentifier) => {
        const updatedApplicationForm = {
            ...this.state.applicationForm
        };
        const updatedFormElement = {
            ...updatedApplicationForm[inputIdentifier]
        };
        updatedFormElement.value = event.target.value;
        updatedFormElement.valid = this.checkValidity(updatedFormElement.value, updatedFormElement.validation);
        updatedFormElement.touched = true;
        updatedApplicationForm[inputIdentifier] = updatedFormElement;

        let formIsValid = true;
        for (let inputIdentifier in updatedApplicationForm) {
            formIsValid = updatedApplicationForm[inputIdentifier].valid && formIsValid;
        }
        this.setState({ applicationForm: updatedApplicationForm, formIsValid: formIsValid });
    }

    // Add new application 
    addNewApplication = () => {
        if (!this.state.formIsValid) {
            // To handle submit with out touching fields
            const applicationFormData = { ...this.state.applicationForm };
            for (let formElementIdentifier in applicationFormData) {
                applicationFormData[formElementIdentifier].touched = true;
            }
            this.setState({ applicationForm: applicationFormData });
            return;
        }

        this.setState({ loading: true });
        const { applicationForm } = this.state;
        const formData = {};
        for (let formElementIdentifier in applicationForm) {
            formData[formElementIdentifier] = applicationForm[formElementIdentifier].value;
        }

        axios.post(APINames.applicationAPI, formData)
            .then(response => {
                if (response && response.status && response.status === 201) {
                    this.hideModal();
                    this.getApplicationsList();
                    this.dismissLoaderAndDisplayMsg(messages.successHeadingMsg, messages.applicationCreateSuccessMsg);
                } else {
                    this.dismissLoaderAndDisplayMsg(messages.errorHeadingMsg, messages.applicationCreateFailedMsg);
                }
            })
            .catch(() => {
                this.dismissLoaderAndDisplayMsg(messages.errorHeadingMsg, messages.serverErrorMsg);
            });
    }

    render() {
        const { modalHeading, modalContent,
            modalButtons, modalData, isOpenModal,
            applicationColumns, applications, loading, isDisplayToast,
            toastContent, toastHeading, applicationForm } = this.state;

        let applicationFormElement = null;
        if (modalHeading === 'Create Application') {
            const formElementsArray = [];
            for (let key in applicationForm) {
                formElementsArray.push({
                    id: key,
                    config: applicationForm[key]
                });
            }
            applicationFormElement = (
                <React.Fragment>
                    <form >
                        {formElementsArray.map(formElement => (
                            <CustomInput
                                label={formElement.config.label}
                                key={formElement.id}
                                elementConfig={formElement.config.elementConfig}
                                value={formElement.config.value}
                                invalid={!formElement.config.valid}
                                shouldValidate={formElement.config.validation}
                                touched={formElement.config.touched}
                                changed={(event) => this.inputChangedHandler(event, formElement.id)} />
                        ))}
                    </form>
                </React.Fragment>
            );
        }

        return (
            <React.Fragment>
                {loading ? <CustomSpinner /> : null}
                <div className="row align-items-center justify-content-between" style={{ padding: '15px' }}>
                    <h3>Applications List
                        <CustomButton className="ml-2 p-0" btnType="outline-primary" btnSize="sm" clicked={this.getApplicationsList}>
                            <i className="material-icons">refresh</i>
                        </CustomButton>
                    </h3>
                    <CustomButton btnType="primary" btnSize="sm" clicked={this.openNewApplicationPopup}>Create Application</CustomButton>
                </div>

                <Table striped bordered hover size="sm" responsive>
                    <thead>
                        <tr>
                            {applicationColumns.map((column, index) => (
                                <th key={index.toString()}>{column.label}</th>
                            ))}
                        </tr>
                    </thead>
                    <tbody>
                        {
                            applications && applications.length ?
                                applications.map((application, applicationIndex) => (
                                    <tr key={applicationIndex.toString()}>
                                        {applicationColumns.map((column, index) => (
                                            <td key={index.toString()}>
                                                {column.value === 'createdAt' ? Moment(application[column.value]).format('L LTS') :
                                                    column.value === 'action' ?
                                                        <CustomButton
                                                            className="p-0"
                                                            btnType="outline-danger"
                                                            btnSize="sm"
                                                            clicked={() => this.askDeleteConfirmation(application)}>
                                                            <i className="material-icons">delete</i>
                                                        </CustomButton>
                                                        : column.value === "serialNo" ? Number(applicationIndex) + 1 : application[column.value]}
                                            </td>
                                        ))}
                                    </tr>
                                ))
                                :
                                <tr>
                                    <td colSpan={applicationColumns.length}>No records found....</td>
                                </tr>
                        }
                        {}
                    </tbody>
                </Table>

                <CustomModal
                    show={isOpenModal}
                    onHide={() => this.hideModal()}
                    heading={modalHeading}
                    content={modalContent}
                    buttons={modalButtons}
                    data={modalData}
                >
                    {modalHeading === 'Create Application' ? applicationFormElement : null}
                    {loading ? <CustomSpinner /> : null}
                </CustomModal>
                <CustomToast
                    show={isDisplayToast}
                    toastContent={toastContent}
                    toastHeading={toastHeading}
                    hideToast={() => this.setState({ isDisplayToast: false })}
                />

            </React.Fragment>
        );
    }
}

export default Applications;
